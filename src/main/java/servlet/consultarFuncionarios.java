/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import DAO.FuncionarioDAO;
import FrontController.Acao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author spyker
 */
public class consultarFuncionarios implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {
        FuncionarioDAO fDAO = new FuncionarioDAO();
        request.setAttribute("listaDeFuncionarios", fDAO.consulta(request.getParameter("pesquisa")));
        return "listaDeFuncionarios.jsp";
    }
}
