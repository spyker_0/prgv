/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import DAO.FuncionarioDAO;
import DTO.FuncionarioDTO;
import FrontController.Acao;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author spyker
 */
public class excluirFuncionario implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) {
        FuncionarioDAO fDAO = new FuncionarioDAO();
        
        fDAO.excluir(Integer.parseInt(request.getParameter("id")));
        
        return "index.html";
    }
}
